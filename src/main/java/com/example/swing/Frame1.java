package com.example.swing;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.Border;

public class Frame1 extends JFrame {

    JFrame frame = new JFrame("Ma Pizzeria");// creating instance of JFrame

    Frame1() {

        ImageIcon img = new ImageIcon("/home/kevin/Bureau/tranche-de-pizza.png");
        frame.setIconImage(img.getImage());

        JLabel automaticCashLabel, pizzaNumberLabel, resultLabel, errorNumericLabel, errorRadioLabel,
                errorPizzaNumberLabel, pizzaPriceLabel;
        JSpinner sauceNumbeSpinner;
        JButton sumButton;
        final JLabel resultNumberLabel;
        final JTextField pizzaNumberTextField;
        final JCheckBox cheeseSupplementCheckbox;
        ButtonGroup deliveryRadioButtonGroup, pizzaChoiceRadioButtonGroup;
        JRadioButton onSiteRadioButton, takeAwayRadioButton, margaritaRadioButton, cannibaleRadioButton,
                calzoneRadioButton;

        automaticCashLabel = new JLabel("Caisse automatique");
        automaticCashLabel.setBounds(130, 20, 200, 20);

        onSiteRadioButton = new JRadioButton("Sur place");
        onSiteRadioButton.setBounds(75, 50, 100, 30);
        takeAwayRadioButton = new JRadioButton("À emporter");
        takeAwayRadioButton.setBounds(175, 50, 150, 30);
        margaritaRadioButton = new JRadioButton("Margarita");
        margaritaRadioButton.setBounds(50, 90, 100, 30);
        cannibaleRadioButton = new JRadioButton("Cannibale");
        cannibaleRadioButton.setBounds(150, 90, 100, 30);
        calzoneRadioButton = new JRadioButton("Calzone");
        calzoneRadioButton.setBounds(250, 90, 100, 30);

        errorRadioLabel = new JLabel("");
        errorRadioLabel.setBounds(100, 75, 250, 20);

        pizzaPriceLabel = new JLabel("Prix de la pizza :");
        pizzaPriceLabel.setBounds(50, 120, 150, 20);

        pizzaNumberLabel = new JLabel("Nombre de pizzas :");
        pizzaNumberLabel.setBounds(50, 150, 150, 20);

        pizzaNumberTextField = new JTextField("");
        pizzaNumberTextField.setBounds(200, 150, 70, 20);

        errorPizzaNumberLabel = new JLabel("");
        errorPizzaNumberLabel.setBounds(50, 170, 250, 20);

        resultLabel = new JLabel("Résultat :");
        resultLabel.setBounds(50, 190, 200, 20);
        resultNumberLabel = new JLabel("");
        resultNumberLabel.setBounds(200, 190, 100, 20);
        errorNumericLabel = new JLabel("");
        errorNumericLabel.setBounds(50, 160, 250, 20);

        Border border = BorderFactory.createLineBorder(Color.BLUE, 2);
        resultLabel.setBorder(border);

        cheeseSupplementCheckbox = new JCheckBox("Supplément Fromage (+1.5€)");
        cheeseSupplementCheckbox.setBounds(50, 230, 250, 30);

        SpinnerModel value = new SpinnerNumberModel(0, // initial value
                0, // minimum value
                100, // maximum value
                1); // step
        sauceNumbeSpinner = new JSpinner(value);
        sauceNumbeSpinner.setBounds(50, 280, 150, 30);
        deliveryRadioButtonGroup = new ButtonGroup();
        deliveryRadioButtonGroup.add(onSiteRadioButton);
        deliveryRadioButtonGroup.add(takeAwayRadioButton);

        pizzaChoiceRadioButtonGroup = new ButtonGroup();
        pizzaChoiceRadioButtonGroup.add(margaritaRadioButton);
        pizzaChoiceRadioButtonGroup.add(cannibaleRadioButton);
        pizzaChoiceRadioButtonGroup.add(calzoneRadioButton);

        frame.add(onSiteRadioButton);
        frame.add(takeAwayRadioButton);

        frame.add(margaritaRadioButton);
        frame.add(cannibaleRadioButton);
        frame.add(calzoneRadioButton);

        frame.add(pizzaPriceLabel);
        frame.add(sauceNumbeSpinner);
        frame.add(automaticCashLabel);
        frame.add(pizzaNumberLabel);
        frame.add(resultLabel);
        frame.add(resultNumberLabel);
        frame.add(errorNumericLabel);
        frame.add(errorPizzaNumberLabel);
        frame.add(errorRadioLabel);
        frame.add(pizzaNumberTextField);
        frame.add(cheeseSupplementCheckbox);

        sumButton = new JButton("Calculer le total");// creating instance of JButton
        sumButton.setBounds(100, 330, 200, 40);// x axis, y axis, width, height
        frame.add(sumButton);// adding button in JFrame

        ItemListener itemListener = new ItemListener() {

            public void itemStateChanged(ItemEvent itemEvent) {
                AbstractButton aButton = (AbstractButton) itemEvent.getSource();
                int state = itemEvent.getStateChange();
                String pizzaRadioButtonSelectedLabel = aButton.getText();
                if (state == ItemEvent.SELECTED) {
                    switch (pizzaRadioButtonSelectedLabel) {
                        case "Margarita":
                            pizzaPriceLabel.setText("Prix de la pizza : " + Margarita.price);
                            break;
                        case "Cannibale":
                            pizzaPriceLabel.setText("Prix de la pizza : " + Cannibale.price);
                            break;
                        case "Calzone":
                            pizzaPriceLabel.setText("Prix de la pizza : " + Calzone.price);
                            break;

                        default:
                            break;
                    }
                }
            }
        };

        margaritaRadioButton.addItemListener(itemListener);
        cannibaleRadioButton.addItemListener(itemListener);
        calzoneRadioButton.addItemListener(itemListener);

        sumButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                if (!onSiteRadioButton.isSelected() && !takeAwayRadioButton.isSelected()) {
                    errorRadioLabel.setText("Sélectionnez une valeur");
                } else if (pizzaNumberTextField.getText().isEmpty()) {
                    errorPizzaNumberLabel.setText("Entrez une valeur");
                } else {
                    errorRadioLabel.setText("");
                    errorPizzaNumberLabel.setText("");
                    double inputPizzaNumber = Integer.parseInt(pizzaNumberTextField.getText());
                    double sum = 0;
                    sum = (inputPizzaNumber * Pizza.price)
                            + (Double.parseDouble(sauceNumbeSpinner.getValue().toString()) * Sauce.price);
                    if (cheeseSupplementCheckbox.isSelected()) {
                        sum += 1.5;
                    }

                    if (takeAwayRadioButton.isSelected()) {
                        sum += 10;
                    }

                    resultNumberLabel.setText(String.valueOf(sum));
                }
            }
        });

        pizzaNumberTextField.addKeyListener(new KeyAdapter() {
            public void keyPressed(KeyEvent key) {
                if (((key.getKeyChar() < '0') || (key.getKeyChar() > '9'))
                        && (key.getKeyChar() != KeyEvent.VK_BACK_SPACE)) {
                    pizzaNumberTextField.setEditable(false);
                    errorNumericLabel.setText("Seul les nombres sont autorisés(0-9)");
                } else {
                    pizzaNumberTextField.setEditable(true);
                    errorNumericLabel.setText("");
                }
            }
        });

        frame.setSize(400, 500);// 400 width and 500 height
        frame.setLayout(null);// using no layout managers
        frame.setVisible(true);// making the frame visible
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

}
