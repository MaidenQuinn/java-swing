package com.example.swing;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

public class TabbedPane extends JFrame {

    JFrame frame = new JFrame("Ma Pizzeria");// creating instance of JFrame
    double sum = 0;

    TabbedPane() {
        ImageIcon img = new ImageIcon("/home/kevin/Bureau/tranche-de-pizza.png");
        frame.setIconImage(img.getImage());

        JPanel newCommandPanel = new JPanel();
        newCommandPanel.setBounds(50, 50, 600, 500);
        JPanel commandsListPanel = new JPanel();
        commandsListPanel.setBounds(50, 50, 600, 500);

        String pizza[] = { "Sélectionnez..", "Margarita", "Cannibale", "Calzone" };

        JTabbedPane tabbedPane = new JTabbedPane();
        tabbedPane.setBounds(50, 50, 600, 500);

        final JComboBox cb = new JComboBox(pizza);
        cb.setBounds(50, 50, 200, 20);

        final JLabel pizzaPriceLabel = new JLabel();
        pizzaPriceLabel.setBounds(270, 50, 300, 20);

        final JLabel pizzaNumberLabel = new JLabel("Entrez le nombre de pizza à commander");
        pizzaNumberLabel.setBounds(50, 80, 300, 20);

        final JTextField pizzaNumberTextField = new JTextField();
        pizzaNumberTextField.setBounds(50, 100, 200, 20);

        final JLabel errorNumericLabel = new JLabel("");
        errorNumericLabel.setBounds(50, 120, 350, 20);

        JButton orderButton = new JButton("Commander");// creating instance of JButton
        orderButton.setBounds(50, 200, 200, 40);// x axis, y axis, width, height

        JTable jt = new JTable();
        jt.setBounds(50, 50, 500, 400);

        DefaultTableModel dtm = new DefaultTableModel(0, 0);
        String header[] = new String[] { "Nom de la pizza", "Quantité", "Prix total" };
        dtm.setColumnIdentifiers(header);
        jt.setModel(dtm);
        JScrollPane sp = new JScrollPane(jt);

        tabbedPane.add("Nouvelle commande", newCommandPanel);
        tabbedPane.add("Liste des commandes", commandsListPanel);
        newCommandPanel.add(cb);
        newCommandPanel.add(pizzaNumberLabel);
        newCommandPanel.add(pizzaNumberTextField);
        newCommandPanel.add(pizzaPriceLabel);
        newCommandPanel.add(errorNumericLabel);
        newCommandPanel.add(orderButton);// adding button in JFrame
        commandsListPanel.add(sp);
        frame.add(tabbedPane);
        newCommandPanel.setLayout(null);

        orderButton.addActionListener((arg0) -> {
            double inputPizzaNumber = pizzaNumberTextField.getText().isEmpty() ? 0
                    : Integer.parseInt(pizzaNumberTextField.getText());
            switch (cb.getSelectedItem().toString()) {
                case "Margarita":
                    sum = inputPizzaNumber * Margarita.price;
                    break;
                case "Cannibale":
                    sum = inputPizzaNumber * Cannibale.price;
                    break;
                case "Calzone":
                    sum = inputPizzaNumber * Calzone.price;
                    break;

                default:
                    break;
            }
            int input = JOptionPane.showConfirmDialog(null,
                    "Souhaitez-vous vraiment commander ? Prix total : " + sum + " €",
                    "Sélectionnez une option...",
                    JOptionPane.YES_NO_OPTION);

            if (input == 0) {
                dtm.addRow(new Object[] { cb.getSelectedItem().toString(),
                        Integer.parseInt(pizzaNumberTextField.getText().toString()),
                        String.valueOf(sum).concat(" €") });
            }

            pizzaNumberTextField.setText("");
            cb.setSelectedIndex(0);
            pizzaPriceLabel.setText("");

        });

        cb.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                switch (cb.getSelectedItem().toString()) {
                    case "Margarita":
                        pizzaPriceLabel.setText("Prix de la pizza : " + Margarita.price);
                        break;
                    case "Cannibale":
                        pizzaPriceLabel.setText("Prix de la pizza : " + Cannibale.price);
                        break;
                    case "Calzone":
                        pizzaPriceLabel.setText("Prix de la pizza : " + Calzone.price);
                        break;

                    default:
                        break;
                }
            }
        });

        pizzaNumberTextField.addKeyListener(new KeyAdapter() {
            public void keyPressed(KeyEvent key) {
                if (((key.getKeyChar() < '0') || (key.getKeyChar() > '9'))
                        && (key.getKeyChar() != KeyEvent.VK_BACK_SPACE)) {
                    pizzaNumberTextField.setEditable(false);
                    errorNumericLabel.setText("Seul les nombres sont autorisés(0-9)");
                } else {
                    pizzaNumberTextField.setEditable(true);
                    errorNumericLabel.setText("");
                }
            }
        });

        frame.setSize(800, 600);
        frame.setLayout(null);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    }

}
